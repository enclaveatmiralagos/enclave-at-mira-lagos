Offering the best in luxury living, nestled around a private lake, Enclave at Mira Lagos is a one of a kind upscale apartment community. Our beautiful, tranquil setting is one of the most sought-after spots in all of Grand Prairie.

Address: 2629 S.Grand Peninsula Dr, Grand Prairie, TX 75054, USA

Phone: 817-310-9455